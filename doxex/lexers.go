package doxex

import (
	"unicode"

	"strings"

	"github.com/wirekit/dox/lexer"
)

// constants of header names.
const (
	importHeader   = "import"
	listModifier   = "list"
	packageHeader  = "package"
	versionHeader  = "version"
	enumHeader     = "enum"
	messsageHeader = "message"
	metaHeader     = "meta"
	embedHeader    = "embed"
)

// constants of token runes.
const (
	eof         = rune(0)
	colon       = rune(':')
	semicolon   = rune(';')
	hasher      = rune('#')
	qmarker     = rune('!')
	newline     = rune('\n')
	breakline   = rune('\r')
	dquote      = rune('"')
	squote      = rune('\'')
	listOpen    = rune('[')
	listEnd     = rune(']')
	curveOpen   = rune('(')
	curveEnd    = rune(')')
	bracketOpen = rune('{')
	bracketEnd  = rune('}')
)

// constants for token types
const (
	ItemMetaBegin lexer.ItemType = iota + 10

	// constants for version values
	ItemVersionBegin
	ItemVersion
	ItemVersionEnd

	// constants for package values
	ItemPackageBegin
	ItemPackageName
	ItemPackageEnd

	ItemImportBegin
	ItemImportTarget
	ItemImportEnd

	ItemMetaEnd

	// constants for comment values
	ItemCommentStart
	ItemComment
	ItemCommentEnd

	ItemDirectiveStart

	// constants for message declarations
	ItemMessage
	ItemMessageStart
	ItemMessageEnd

	// constants for enum declarations
	ItemEnum
	ItemEnumBodyStart
	ItemEnumBodyEnd

	// constants for fields declarations
	ItemFieldStart
	ItemFieldName
	ItemFieldValue
	ItemFieldEnd

	ItemDirectiveEnd
)

// TokenMapper defines a function that returns appropriate
// string representation of a giving set of token types.
func TokenMapper(t lexer.ItemType) string {
	switch t {
	case ItemImportBegin:
		return "[ImportBegin]"
	case ItemImportEnd:
		return "[ImportEnd]"
	case ItemCommentStart:
		return "[CommentStart]"
	case ItemCommentEnd:
		return "[CommentEnd]"
	case ItemFieldStart:
		return "[FieldStart]"
	case ItemFieldEnd:
		return "[FieldEnd]"
	case ItemMessageStart:
		return "[MessageStart]"
	case ItemMessageEnd:
		return "[MessageEnd]"
	case ItemEnumBodyStart:
		return "[EnumBodyStart]"
	case ItemEnumBodyEnd:
		return "[EnumBodyEnd]"
	default:
		return ""
	}
}

//************************************************
// Parser
//************************************************

// Parser defines an interface which exposes a parser for
// the dox message format.
type Parser interface {
	NextToken() (t lexer.LexToken, finished bool)
}

// New returns a new instance of a type implementing Parser.
func New(name string, source string) Parser {
	return lexer.NewSyncLexer(name, source, LexDirective, TokenMapper)
}

//************************************************
// Lex Parsing State Functions
//************************************************

// LexSkip will skip current consumed tokens and return next state
// when called.
func LexSkip(l *lexer.Lexer, next lexer.NextState) lexer.NextState {
	return func(l *lexer.Lexer) lexer.NextState {
		l.Ignore()
		return next
	}
}

// LexDo returns a NextState that will emit giving type and return nil after emission.
func LexDo(t lexer.ItemType, n lexer.NextState) lexer.NextState {
	return func(l *lexer.Lexer) lexer.NextState {
		l.Emit(t)
		return n
	}
}

// LexChain will continuously chain the ender NextState into the calls of the next
// till the next returns a nil state where by the ender then gets returned.
// This allows us do a push pop behaviour in between states. LexChain ensures
// an error emitted is respected and will ignore ender if an error did occur.
func LexChain(l *lexer.Lexer, next lexer.NextState, ender lexer.NextState) lexer.NextState {
	return func(l *lexer.Lexer) lexer.NextState {
		if nx := next(l); nx != nil {
			return LexChain(l, nx, ender)
		}

		if l.Error() == nil {
			return ender
		}

		return nil
	}
}

//*********************************
// Directive Router
//*********************************

const (
	symbols             = "~`|!@$%^&*()[]<>?-+,/"
	variableAntiSymbols = "~`|!@$%^&*()<>?-+,/"
	decimalRunes        = lexer.Digits + "."
	alphaNumerics       = lexer.LettersLowerAndUpperCase + lexer.Digits
	alphaRunes          = lexer.LettersLowerAndUpperCase + lexer.Digits + "_"
	fieldsRunes         = lexer.LettersLowerAndUpperCase + lexer.Digits + "!"
	variableRunes       = lexer.LettersLowerAndUpperCase + lexer.Digits + "_" + ":"
	variableValueRunes  = lexer.LettersLowerAndUpperCase + lexer.Digits + "." + "\"" + "'" + "()[]"
)

// LexDirective attempts to tokenize a possible directive such has variables, meta, enums and messages
// else reverting back to comments tokenization or returning an error if an invalid token is seen.
func LexDirective(l *lexer.Lexer) lexer.NextState {
	// We need to attempt read all possible valid
	// text that can represent the ff:
	// 1. A declaration e.g version: 1.0 (must always have the string end with ':' else is invalid).
	// 2. A enum directive.
	// 3. A message directive.

	r := l.Next()
	if r == eof {
		l.Emit(lexer.ItemEOF)
		return nil
	}

	if unicode.IsSpace(r) {
		l.Ignore()
		return LexDirective
	}

	if r == hasher {
		l.Emit(ItemCommentStart)
		return lexComments
	}

	return lexDeclaration
}

func lexDeclaration(l *lexer.Lexer) lexer.NextState {
	switch r := l.Next(); {
	case r == eof:
		l.Emit(lexer.ItemEOF)
		return nil
	case hasAny(alphaRunes, r):
		return lexDeclaration
	case r == hasher:
		l.Emit(ItemCommentStart)
		return lexComment
	case r == curveOpen:
		l.Backup()
		consumed := l.PeekConsumed()

		switch consumed {
		case versionHeader:
			l.Emit(ItemVersionBegin)
			l.Next()
			l.Ignore()
			return lexVersion
		case packageHeader:
			l.Emit(ItemPackageBegin)
			l.Next()
			l.Ignore()
			return lexPackage
		case importHeader:
			l.Emit(ItemImportBegin)
			l.Next()
			l.Ignore()
			return lexImport
		default:
			l.Next()
			return l.EmitError("invalid directive found: %q", l.PeekConsumed())
		}
	case unicode.IsSpace(r):
		l.Backup()
		consumed := l.PeekConsumed()

		switch consumed {
		case enumHeader:
			l.Next()
			l.Ignore()
			return lexEnum
		case messsageHeader:
			l.Next()
			l.Ignore()
			return lexMessage
		}

		return l.EmitError("invalid directive name (accepts only meta, message and enums, root variables)")
	}

	return lexDeclaration
}

//*********************************
// Comments
//*********************************

// lexComments accepts to locate comment blocks (text starting with #)
// within the current text of the lexer, ignoring all white spaces and
// new lines found. It consumes said text or returns appropriate NextState
// if no comment is found. It is adequate to be used as a initial state.
// It assumes the # has not being consumed.
func lexComments(l *lexer.Lexer) lexer.NextState {
	return LexChain(l, lexComment, LexDo(ItemCommentEnd, LexDirective))
}

// lexEnumComments is used to parse comments within the body of an enum.
func lexEnumComments(l *lexer.Lexer) lexer.NextState {
	return LexChain(l, lexComment, LexDo(ItemCommentEnd, lexEnumBody))
}

// lexMessageComments is used to parse comments within the body of an message.
func lexMessageComments(l *lexer.Lexer) lexer.NextState {
	return LexChain(l, lexComment, LexDo(ItemCommentEnd, lexMessageBody))
}

func lexComment(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if isNewLine(r) || r == eof {
		l.Backup()
		l.Emit(ItemComment)
		l.Next()
		l.Ignore()
		return nil
	}

	return lexComment
}

//*********************************
// Packages
//*********************************

func lexPackage(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r != dquote {
		return l.EmitError("package content must start with quotation using '\"'")
	}

	return lexPackageContent
}

func lexPackageContent(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r == dquote {
		l.Emit(ItemPackageName)
		if l.Next() != curveEnd {
			return l.EmitError("package content must be closed with a curve bracket ')'")
		}

		l.Ignore()
		l.Emit(ItemPackageEnd)
		return LexDirective
	}

	if isNewLine(r) || r == eof {
		return l.EmitError("package content must be enclosed in quotation using '\"'")
	}

	if !hasAny(lexer.LettersLowerCase, r) {
		return l.EmitError("package content must be only small letters")
	}

	return lexPackageContent
}

//*********************************
// Versions
//*********************************

func lexVersion(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r != dquote {
		return l.EmitError("version content must start with quotation using '\"'")
	}

	return lexVersionContent
}

func lexVersionContent(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r == dquote {
		l.Emit(ItemVersion)
		if l.Next() != curveEnd {
			return l.EmitError("version content must be closed with a curve bracket ')'")
		}

		l.Ignore()
		l.Emit(ItemVersionEnd)
		return LexDirective
	}

	if isNewLine(r) || r == eof {
		return l.EmitError("version content must be enclosed in quotation using '\"'")
	}

	if !hasAny(decimalRunes, r) {
		return l.EmitError("version content must decimals")
	}

	return lexVersionContent
}

//*********************************
// Imports
//*********************************

func lexImport(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r != dquote {
		return l.EmitError("import content must start with quotation using '\"'")
	}

	return lexImportContent
}

func lexImportContent(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r == dquote {
		l.Emit(ItemImportTarget)
		if l.Next() != curveEnd {
			return l.EmitError("import content must be closed with a curve bracket ')'")
		}

		l.Ignore()
		l.Emit(ItemImportEnd)
		return LexDirective
	}

	if isNewLine(r) || r == eof {
		return l.EmitError("import content must be enclosed in quotation using '\"'")
	}

	return lexImportContent
}

//*********************************
// Header declarations
//*********************************

func lexHeader(l *lexer.Lexer, t lexer.ItemType, n lexer.NextState) lexer.NextState {
	if l.Error() != nil {
		return nil
	}

	r := l.Next()
	if isWhiteSpace(r) {
		l.Ignore()
		return lexHeader(l, t, n)
	}

	if r == eof {
		return l.EmitError("header must not end with file")
	}

	if isNewLine(r) {
		return l.EmitError("header must not end with newline")
	}

	if r == bracketOpen {
		return n
	}

	// pull out name of header.
	l.Backup()
	return lexName(l, func(li *lexer.Lexer) lexer.NextState {
		li.Emit(t)
		li.Ignore()
		return lexHeader(li, t, n)
	})
}

func lexName(l *lexer.Lexer, n lexer.NextState) lexer.NextState {
	r := l.Next()
	if hasAny(alphaRunes, r) {
		return lexName(l, n)
	}

	l.Backup()
	return n
}

//*********************************
// Field declarations
//*********************************

// lexFieldBy handles parsing a filed with alphanumeric names and values.
func lexFieldBy(li *lexer.Lexer, next lexer.NextState) lexer.NextState {
	return func(l *lexer.Lexer) lexer.NextState {
		r := l.Next()
		if r == eof {
			return l.EmitError("field name can not end in EOF")
		}

		if isNewLine(r) {
			return l.EmitError("field name can start with newline")
		}

		if isWhiteSpace(r) {
			l.Backup()
			l.Emit(ItemFieldName)
			l.Next()
			l.Ignore()

			return next
		}

		if !hasAny(fieldsRunes, r) {
			return l.EmitError("only %q characters allowed in field name", fieldsRunes)
		}

		return lexFieldBy(li, next)
	}
}

// lexField handles parsing all alphanumeric names and alphanumeric, symbol, spaced values.
func lexField(l *lexer.Lexer) lexer.NextState {
	return lexFieldBy(l, lexFieldBody)
}

// lexFieldNumberics handles parsing non-separated  numeric only values.
func lexFieldNumbers(l *lexer.Lexer) lexer.NextState {
	return lexFieldBy(l, lexFieldNumbersBody)
}

// lexFieldAlphaNum handles parsing non-separated alphanumeric names and values.
func lexFieldAlphaNum(l *lexer.Lexer) lexer.NextState {
	return lexFieldBy(l, lexFieldAlphaNumBody)
}

// lexFieldAlphaNumSpaced handles parsing a space separated alphanumeric values.
func lexFieldAlphaNumSpaced(l *lexer.Lexer) lexer.NextState {
	return lexFieldBy(l, lexFieldAlphaNumSpacedBody)
}

// lexFieldBody parses the value of a field and only all values of alphanumerics and symbols.
func lexFieldBody(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r == eof {
		return l.EmitError("field body can not end in eof")
	}

	if isNewLine(r) {
		l.Backup()
		l.Emit(ItemFieldValue)
		l.Emit(ItemFieldEnd)
		l.Next()
		l.Ignore()
		return nil
	}

	if r == hasher {
		l.Backup()
		l.Emit(ItemFieldValue)
		l.Emit(ItemFieldEnd)
		return nil
	}

	if isWhiteSpace(r) {
		return lexFieldBody
	}

	return lexFieldBody
}

// lexFieldAlphaNumSpacedBody parses the value of a field and only all values of alphanumerics only.
func lexFieldAlphaNumSpacedBody(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r == eof {
		return l.EmitError("field body can not end in eof")
	}

	if isNewLine(r) {
		l.Backup()
		l.Emit(ItemFieldValue)
		l.Emit(ItemFieldEnd)
		l.Next()
		l.Ignore()
		return nil
	}

	if r == hasher {
		l.Backup()
		l.Emit(ItemFieldValue)
		l.Emit(ItemFieldEnd)
		return nil
	}

	if isWhiteSpace(r) {
		return lexFieldAlphaNumSpacedBody
	}

	if !hasAny(alphaNumerics, r) {
		return l.EmitError("field body can only be numbers")
	}

	return lexFieldAlphaNumSpacedBody
}

// lexFieldAlphaNumBody parses the value of a field and only all values of alphanumerics only.
func lexFieldAlphaNumBody(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r == eof {
		return l.EmitError("field body can not end in eof")
	}

	if isNewLine(r) {
		l.Backup()
		l.Emit(ItemFieldValue)
		l.Emit(ItemFieldEnd)
		l.Next()
		l.Ignore()
		return nil
	}

	if isWhiteSpace(r) {
		l.Backup()
		l.Emit(ItemFieldValue)
		l.Emit(ItemFieldEnd)
		return nil
	}

	if !hasAny(alphaNumerics, r) {
		return l.EmitError("field body can only be numbers")
	}

	return lexFieldAlphaNumBody
}

func lexFieldNumbersBody(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if r == eof {
		return l.EmitError("field body can not end in eof")
	}

	if isNewLine(r) {
		l.Backup()
		l.Emit(ItemFieldValue)
		l.Emit(ItemFieldEnd)
		l.Next()
		l.Ignore()
		return nil
	}

	if isWhiteSpace(r) {
		l.Backup()
		l.Emit(ItemFieldValue)
		l.Emit(ItemFieldEnd)
		return nil
	}

	if !hasAny(lexer.Digits, r) {
		return l.EmitError("field body can only be numbers: %q ", r)
	}

	return lexFieldNumbersBody
}

//*********************************
// Enums declarations
//*********************************

// lexEnum attempts to tokenize a declared enum type.
func lexEnum(l *lexer.Lexer) lexer.NextState {
	// pull header of enum.
	return lexHeader(l, ItemEnum, func(li *lexer.Lexer) lexer.NextState {
		li.Emit(ItemEnumBodyStart)
		li.Ignore()
		return lexEnumBody
	})
}

func lexEnumBody(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if isNewLine(r) {
		l.Ignore()
		return lexEnumBody
	}

	if isWhiteSpace(r) {
		l.Ignore()
		return lexEnumBody
	}

	if r == hasher {
		l.Emit(ItemCommentStart)
		return lexEnumComments
	}

	if r == bracketEnd {
		l.Emit(ItemEnumBodyEnd)
		l.Ignore()
		return LexDirective
	}

	if hasAny(fieldsRunes, r) {
		l.Backup()
		l.Emit(ItemFieldStart)
		return LexChain(l, lexFieldNumbers, lexEnumBody)
	}

	return lexEnumBody
}

//*********************************
// Message declarations
//*********************************

// lexMessage attempts to tokenize a declared message type.
func lexMessage(l *lexer.Lexer) lexer.NextState {
	// pull header of message.
	return lexHeader(l, ItemMessage, func(li *lexer.Lexer) lexer.NextState {
		li.Emit(ItemMessageStart)
		li.Ignore()
		return lexMessageBody
	})
}

func lexMessageBody(l *lexer.Lexer) lexer.NextState {
	r := l.Next()
	if isNewLine(r) {
		l.Ignore()
		return lexMessageBody
	}

	if isWhiteSpace(r) {
		l.Ignore()
		return lexMessageBody
	}

	if r == hasher {
		l.Emit(ItemCommentStart)
		return lexMessageComments
	}

	if r == bracketEnd {
		l.Emit(ItemMessageEnd)
		l.Ignore()
		return LexDirective
	}

	if hasAny(fieldsRunes, r) {
		l.Backup()
		l.Emit(ItemFieldStart)
		return LexChain(l, lexFieldAlphaNumSpaced, lexMessageBody)
	}

	return lexMessageBody
}

//*********************************
// Utils
//*********************************

func isWhiteSpace(r rune) bool {
	return r == ' ' || r == '\t'
}

func hasAny(s string, r rune) bool {
	return strings.IndexRune(s, r) >= 0
}

func isNewLine(r rune) bool {
	return r == newline || r == breakline
}

func capitalize(s string) string {
	return strings.ToUpper(s[:1]) + s[1:]
}

//// LexVarValueDeclaration attempts to tokenize a declared root variable value section.
//func LexVarValueDeclaration(l *lexer.Lexer) lexer.NextState {
//	var foundValueStart bool
//
//	for {
//		nx := l.Next()
//		if hasAny(variableAntiSymbols, nx) && !foundValueStart {
//			return l.EmitError("invalid symbol/char found during variable declaration parsing: %+q", nx)
//		}
//
//		if unicode.IsSpace(nx) {
//			if foundValueStart {
//				// move back a step
//				l.Backup()
//
//				// Emit Variable declaration token.
//				l.Emit(ItemVarStart)
//
//				// Attempt to parse possible variable comment.
//				// LexType knows what to do if no comment appears.
//				return LexTypeComment(ItemDeclrComment)
//			}
//
//			continue
//		}
//
//		if hasAny(variableValueRunes, nx) {
//			if foundValueStart {
//				continue
//			}
//
//			// if we are yet to start to see a value and we
//			// are already seeing special characters like this set
//			// then that's illegal.
//			if hasAny("._-'", nx) {
//				return l.EmitError("illegal character found starting variable value: %+q", nx)
//			}
//
//			// if we are starting with a bracket('[') then its possible an array,
//			// so attempt to parse value out.
//			if nx == listOpen {
//				// if we have a hash after ending of the list due to
//				// variable comment, then validate if there is a quotation suffix ending text
//				// and emit that.
//				if quotedWithHashEnding := l.PeekIfOnly(hasher); quotedWithHashEnding != "" {
//					quotedWithHashEnding = strings.TrimSpace(quotedWithHashEnding)
//
//					if !strings.HasSuffix(quotedWithHashEnding, "]") {
//						return l.EmitError("illegal ending of list variable value: %+q", quotedWithHashEnding)
//					}
//
//					l.Jump(utf8.RuneCountInString(quotedWithHashEnding))
//					l.Emit(ItemVarStart)
//					return LexTypeComment(ItemDeclrComment)
//				}
//
//				// if we have a newline ending the bracket, then validate, there is a quotation suffix ending text.
//				if quotedWithLineEnding := l.PeekIfOnly(newline); quotedWithLineEnding != "" {
//					if !strings.HasSuffix(quotedWithLineEnding, "]") {
//						return l.EmitError("illegal ending of list variable value: %+q", quotedWithLineEnding)
//					}
//
//					l.Jump(utf8.RuneCountInString(quotedWithLineEnding))
//					l.Emit(ItemVarStart)
//					return LexComments
//				}
//
//				// if we have EOF ending quote, then validate there is a quotation suffix ending the text.
//				if quotedWithEOFEnding := l.PeekTill(eof); quotedWithEOFEnding != "" {
//					if !strings.HasSuffix(quotedWithEOFEnding, "]") {
//						return l.EmitError("illegal ending of list variable value: %+q", quotedWithEOFEnding)
//					}
//
//					l.Jump(utf8.RuneCountInString(quotedWithEOFEnding))
//					l.Emit(ItemVarStart)
//					l.Emit(lexer.ItemEOF)
//					return nil
//				}
//
//				return l.EmitError("illegal ending of variable value: %+q", l.PeekConsumed())
//			}
//
//			// if we are start with a quote then, we are play a simple
//			// trick by scanning/peeking forward till we meet a quote and a
//			// newline i.e "\n, has new lines are the statement delimiter.
//			// Yes that means internally double quoted characters are
//			// impossible, if quoting use single quotation.
//			if nx == dquote {
//				// if we have a hash after ending quote due to
//				// variable comment, then validate if there is a quotation suffix ending text
//				// and emit that.
//				if quotedWithHashEnding := l.PeekIfOnly(hasher); quotedWithHashEnding != "" {
//					quotedWithHashEnding = strings.TrimSpace(quotedWithHashEnding)
//
//					if !strings.HasSuffix(quotedWithHashEnding, "\"") {
//						return l.EmitError("illegal ending of quoted variable value: %+q", quotedWithHashEnding)
//					}
//
//					l.Jump(utf8.RuneCountInString(quotedWithHashEnding))
//					l.Emit(ItemVarStart)
//					return LexTypeComment(ItemDeclrComment)
//				}
//
//				// if we have a newline ending the quote, then validate, there is a quotation suffix ending text.
//				if quotedWithLineEnding := l.PeekIfOnly(newline); quotedWithLineEnding != "" {
//					if !strings.HasSuffix(quotedWithLineEnding, "\"") {
//						return l.EmitError("illegal ending of quoted variable value: %+q", quotedWithLineEnding)
//					}
//
//					l.Jump(utf8.RuneCountInString(quotedWithLineEnding))
//					l.Emit(ItemVarStart)
//					return LexComments
//				}
//
//				// if we have EOF ending quote, then validate there is a quotation suffix ending the text.
//				if quotedWithEOFEnding := l.PeekTill(eof); quotedWithEOFEnding != "" {
//					if !strings.HasSuffix(quotedWithEOFEnding, "\"") {
//						return l.EmitError("illegal ending of quoted variable value: %+q", quotedWithEOFEnding)
//					}
//
//					l.Jump(utf8.RuneCountInString(quotedWithEOFEnding))
//					l.Emit(ItemVarStart)
//					l.Emit(lexer.ItemEOF)
//					return nil
//				}
//
//				return l.EmitError("illegal ending of variable value: %+q", l.PeekConsumed())
//			}
//
//			foundValueStart = true
//			continue
//		}
//
//		if semicolon == nx {
//			if !foundValueStart {
//				return l.EmitError("illegal character found starting variable value: %+q", nx)
//			}
//
//			// if we have a semiconlon, then possible use of ending delimiter, check if next is newline, then
//			// emit variable and ignore.
//			if l.Peek() == newline {
//				l.Backup()
//				l.Emit(ItemVarStart)
//				l.Next()
//				l.Ignore()
//				return LexComments
//			}
//		}
//
//		if hasher == nx {
//			// if we meet a comment hash but fail to see any character before then
//			// then its an error in format.
//			if !foundValueStart {
//				return l.EmitError("expected value for variable declaration before variable comment")
//			}
//
//			// move back a step
//			l.Backup()
//
//			// Emit Variable declaration token.
//			l.Emit(ItemVarStart)
//
//			return LexTypeComment(ItemDeclrComment)
//		}
//
//		if newline == nx {
//			// if we meet a new line but fail to see any character before then
//			// then its an error in format.
//			if !foundValueStart {
//				return l.EmitError("expected value for variable declaration before next line")
//			}
//
//			l.Backup()
//
//			// Emit Variable declaration token.
//			l.Emit(ItemVarStart)
//
//			// return to comment parsing state.
//			return LexComments
//		}
//
//		if nx == eof {
//			if foundValueStart {
//				// move back a step
//				l.Backup()
//
//				// Emit Variable declaration token.
//				l.Emit(ItemVarStart)
//				l.Emit(lexer.ItemEOF)
//				return nil
//			}
//
//			return l.EmitError("illegal ending of variable declaration %+q", l.PeekConsumed())
//		}
//	}
//
//	return nil
//}

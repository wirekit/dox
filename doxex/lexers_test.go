package doxex

import (
	"testing"

	"github.com/influx6/faux/tests"
	"github.com/wirekit/dox/lexer"
)

type TestSpec struct {
	Name         string
	Input        string
	FailExpected bool
	Expected     []lexer.LexToken
}

var (
	doxexRes = []lexer.LexToken{
		{Type: 21, Value: "#"},
		{Type: 22, Value: " voxa spec is a indent based message protocol mapper that transforms"},
		{Type: 23, Value: ""},
		{Type: 21, Value: "#"},
		{Type: 22, Value: " type specs into json hash tables with relationships. This then allows "},
		{Type: 23, Value: ""},
		{Type: 21, Value: "#"},
		{Type: 22, Value: " different generators to take that spec to be used for code generation"},
		{Type: 23, Value: ""},
		{Type: 21, Value: "#"},
		{Type: 22, Value: " in various languages."},
		{Type: 23, Value: ""},
		{Type: 14, Value: "package"},
		{Type: 15, Value: "\"waxmodels\""},
		{Type: 16, Value: ""},
		{Type: 11, Value: "version"},
		{Type: 12, Value: "\"1.0\""},
		{Type: 13, Value: ""},
		{Type: 17, Value: "import"},
		{Type: 18, Value: "\"models/wax.voxa\""},
		{Type: 19, Value: ""},
		{Type: 28, Value: "Messages"},
		{Type: 29, Value: "{"},
		{Type: 31, Value: ""},
		{Type: 32, Value: "Flat"},
		{Type: 33, Value: "1"},
		{Type: 34, Value: ""},
		{Type: 21, Value: "#"},
		{Type: 22, Value: " Flat represents the information regarding a flat message."},
		{Type: 23, Value: ""},
		{Type: 31, Value: ""},
		{Type: 32, Value: "Undertones"},
		{Type: 33, Value: "2"},
		{Type: 34, Value: ""},
		{Type: 30, Value: "}"},
		{Type: 25, Value: "State"},
		{Type: 26, Value: "{"},
		{Type: 31, Value: ""},
		{Type: 32, Value: "id"},
		{Type: 33, Value: "string 1"},
		{Type: 34, Value: ""},
		{Type: 31, Value: ""},
		{Type: 32, Value: "code"},
		{Type: 33, Value: "int32 2"},
		{Type: 34, Value: ""},
		{Type: 27, Value: "}"},
		{Type: 25, Value: "Lex"},
		{Type: 26, Value: "{"},
		{Type: 31, Value: ""},
		{Type: 32, Value: "Date"},
		{Type: 33, Value: "time 1"},
		{Type: 34, Value: ""},
		{Type: 27, Value: "}"},
		{Type: 21, Value: "#"},
		{Type: 22, Value: " message are the only structures available and define what a given"},
		{Type: 23, Value: ""},
		{Type: 21, Value: "#"},
		{Type: 22, Value: " type contains. All field names begin the internal structure of the "},
		{Type: 23, Value: ""},
		{Type: 21, Value: "#"},
		{Type: 22, Value: " type and are followed by their type and tag number."},
		{Type: 23, Value: ""},
		{Type: 25, Value: "Hello"},
		{Type: 26, Value: "{"},
		{Type: 31, Value: ""},
		{Type: 32, Value: "!name"},
		{Type: 33, Value: "string 3"},
		{Type: 34, Value: ""},
		{Type: 31, Value: ""},
		{Type: 32, Value: "age"},
		{Type: 33, Value: "int 2"},
		{Type: 34, Value: ""},
		{Type: 31, Value: ""},
		{Type: 32, Value: "addresses"},
		{Type: 33, Value: "list string 1"},
		{Type: 34, Value: ""},
		{Type: 31, Value: ""},
		{Type: 32, Value: "status"},
		{Type: 33, Value: "enum Messages 4"},
		{Type: 34, Value: ""},
		{Type: 31, Value: ""},
		{Type: 32, Value: "state"},
		{Type: 33, Value: "message State 5"},
		{Type: 34, Value: ""},
		{Type: 31, Value: ""},
		{Type: 32, Value: "codes"},
		{Type: 33, Value: "list message Lex 6"},
		{Type: 34, Value: ""},
		{Type: 27, Value: "}"},
		{Type: 1, Value: ""},
	}
)

func TestDoxexLexingWithSyncLexer(t *testing.T) {
	tests.Header("When parsing doxex message")
	{
		var specs = []TestSpec{
			{
				Name: "the models doxex format message",
				Input: `
					# voxa spec is a indent based message protocol mapper that transforms
					# type specs into json hash tables with relationships. This then allows 
					# different generators to take that spec to be used for code generation
					# in various languages.

					package("waxmodels") 
					version("1.0")
					import("models/wax.voxa")

					enum Messages {
					  Flat 1  # Flat represents the information regarding a flat message.
					  Undertones 2
					}

					message State {
					  id string 1
					  code int32 2
					}

					message Lex {
					  Date time 1
					} 

					# message are the only structures available and define what a given
					# type contains. All field names begin the internal structure of the 
					# type and are followed by their type and tag number.
					message Hello {
					  !name string 3
					  age int 2
					  addresses list string 1
					  status enum Messages 4
					  state message State 5
					  codes list message Lex 6
					}
				`,
				Expected: doxexRes,
			},
		}

		for _, spec := range specs {
			tests.Header("Running: %+q", spec.Name)

			lx := lexer.NewSyncLexer(spec.Name, spec.Input, LexDirective, TokenMapper)

			var res []lexer.LexToken
			var hasError bool

			for {
				item, done := lx.NextToken()
				res = append(res, item)

				if item.Type == lexer.ItemError {
					hasError = true
				}

				if done {
					break
				}
			}

			if hasError {
				tests.Failed("Should have successfully parsed all tokens")
			}

			if !deepCompare(spec.Expected, res) {
				tests.Info("Received Tokens: %+q\n", res)
				tests.Info("Expected Tokens: %+q\n", spec.Expected)
				tests.Failed("Should have received expected tokens from lexer")
			}

			tests.Passed("Should have received expected tokens from lexer")
		}
	}
}

func TestDoxexLexing(t *testing.T) {
	tests.Header("When parsing doxex message")
	{
		var specs = []TestSpec{
			{
				Name: "the models doxex format message",
				Input: `
					# voxa spec is a indent based message protocol mapper that transforms
					# type specs into json hash tables with relationships. This then allows 
					# different generators to take that spec to be used for code generation
					# in various languages.

					package("waxmodels") 
					version("1.0")
					import("models/wax.voxa")

					enum Messages {
					  Flat 1  # Flat represents the information regarding a flat message.
					  Undertones 2
					}

					message State {
					  id string 1
					  code int32 2
					}

					message Lex {
					  Date time 1
					} 

					# message are the only structures available and define what a given
					# type contains. All field names begin the internal structure of the 
					# type and are followed by their type and tag number.
					message Hello {
					  !name string 3
					  age int 2
					  addresses list string 1
					  status enum Messages 4
					  state message State 5
					  codes list message Lex 6
					}
				`,
				Expected: doxexRes,
			},
		}

		for _, spec := range specs {
			tests.Header("Running: %+q", spec.Name)

			tokens := make(chan lexer.LexToken, 0)
			lx := lexer.NewAsyncLexer(spec.Name, spec.Input, tokens, TokenMapper)
			go lx.Run(LexDirective)

			var res []lexer.LexToken
			var hasError bool

			for item := range tokens {
				if item.Type == lexer.ItemError {
					hasError = true
				}

				res = append(res, item)
			}

			if hasError {
				tests.Failed("Should have successfully parsed all tokens")
			}

			if !deepCompare(spec.Expected, res) {
				tests.Info("Received Tokens: %+q\n", res)
				tests.Info("Expected Tokens: %+q\n", spec.Expected)
				tests.Failed("Should have received expected tokens from lexer")
			}

			tests.Passed("Should have received expected tokens from lexer")
		}
	}
}

func TestDoxex_CommentsDeclaration(t *testing.T) {
	tests.Header("When parsing comments using the doxex format")
	{
		var specs = []TestSpec{
			{
				Name:  "single block comments",
				Input: "# we came a long way down",
				Expected: []lexer.LexToken{
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " we came a long way down",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name:  "double block comments",
				Input: "# we came a long way down\n# we came and saw",
				Expected: []lexer.LexToken{
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " we came a long way down",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " we came and saw",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "double block break-line comments",
				Input: `
						# we came a long way down
						# we came and saw
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " we came a long way down",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " we came and saw",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "attempt comments without hash",
				Input: `
						 we came a long way down
				`,
				FailExpected: true,
				Expected: []lexer.LexToken{
					{
						Type:  lexer.ItemError,
						Value: "invalid directive name (accepts only meta, message and enums, root variables)",
					},
				},
			},
		}

		for _, spec := range specs {
			tests.Header("Running: %+q", spec.Name)

			tokens := make(chan lexer.LexToken, 0)
			lx := lexer.NewAsyncLexer(spec.Name, spec.Input, tokens, TokenMapper)
			go lx.Run(LexDirective)

			var res []lexer.LexToken

			for item := range tokens {
				res = append(res, item)
			}

			if spec.FailExpected {
				if !deepCompare(spec.Expected, res) {
					tests.Info("Received Tokens: %+q\n", res)
					tests.Info("Expected Tokens: %+q\n", spec.Expected)
					tests.Failed("Should have failed with expected tokens from lexer")
				}

				tests.Passed("Should have failed with expected tokens from lexer")
				continue
			}

			if !deepCompare(spec.Expected, res) {
				tests.Info("Received Tokens: %+q\n", res)
				tests.Info("Expected Tokens: %+q\n", spec.Expected)
				tests.Failed("Should have received expected tokens from lexer")
			}
			tests.Passed("Should have received expected tokens from lexer")
		}
	}
}

func TestDoxex_PackageDeclaration(t *testing.T) {
	tests.Header("When parsing package directive using the doxex format")
	{
		var specs = []TestSpec{
			{
				Name:  "declare package directive",
				Input: `package("models")`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemPackageBegin,
						Mapper: TokenMapper,
						Value:  "package",
					},
					{
						Type:   ItemPackageName,
						Mapper: TokenMapper,
						Value:  `"models"`,
					},
					{
						Mapper: TokenMapper,
						Type:   ItemPackageEnd,
						Value:  "",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
		}

		for _, spec := range specs {
			tests.Header("Running: %+q", spec.Name)

			tokens := make(chan lexer.LexToken, 0)
			lx := lexer.NewAsyncLexer(spec.Name, spec.Input, tokens, TokenMapper)
			go lx.Run(LexDirective)

			var res []lexer.LexToken

			for item := range tokens {
				res = append(res, item)
			}

			if spec.FailExpected {
				if !deepCompare(spec.Expected, res) {
					tests.Info("Received Tokens: %+q\n", res)
					tests.Info("Expected Tokens: %+q\n", spec.Expected)
					tests.Failed("Should have failed with expected tokens from lexer")
				}

				tests.Passed("Should have failed with expected tokens from lexer")
				continue
			}

			if !deepCompare(spec.Expected, res) {
				tests.Info("Received Tokens: %+q\n", res)
				tests.Info("Expected Tokens: %+q\n", spec.Expected)
				tests.Failed("Should have received expected tokens from lexer")
			}
			tests.Passed("Should have received expected tokens from lexer")
		}
	}
}

func TestDoxex_VersionDeclaration(t *testing.T) {
	tests.Header("When parsing version directive using the doxex format")
	{
		var specs = []TestSpec{
			{
				Name:  "declare version directive",
				Input: `version("2.0")`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemVersionBegin,
						Mapper: TokenMapper,
						Value:  "version",
					},
					{
						Type:   ItemVersion,
						Mapper: TokenMapper,
						Value:  `"2.0"`,
					},
					{
						Mapper: TokenMapper,
						Type:   ItemVersionEnd,
						Value:  "",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
		}

		for _, spec := range specs {
			tests.Header("Running: %+q", spec.Name)

			tokens := make(chan lexer.LexToken, 0)
			lx := lexer.NewAsyncLexer(spec.Name, spec.Input, tokens, TokenMapper)
			go lx.Run(LexDirective)

			var res []lexer.LexToken

			for item := range tokens {
				res = append(res, item)
			}

			if spec.FailExpected {
				if !deepCompare(spec.Expected, res) {
					tests.Info("Received Tokens: %+q\n", res)
					tests.Info("Expected Tokens: %+q\n", spec.Expected)
					tests.Failed("Should have failed with expected tokens from lexer")
				}

				tests.Passed("Should have failed with expected tokens from lexer")
				continue
			}

			if !deepCompare(spec.Expected, res) {
				tests.Info("Received Tokens: %+q\n", res)
				tests.Info("Expected Tokens: %+q\n", spec.Expected)
				tests.Failed("Should have received expected tokens from lexer")
			}
			tests.Passed("Should have received expected tokens from lexer")
		}
	}
}

func TestDoxex_ImportDeclaration(t *testing.T) {
	tests.Header("When parsing import directive using the doxex format")
	{
		var specs = []TestSpec{
			{
				Name:  "declare import directive",
				Input: `import("./box/widget/moc.doxa")`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemImportBegin,
						Mapper: TokenMapper,
						Value:  "import",
					},
					{
						Type:   ItemImportTarget,
						Mapper: TokenMapper,
						Value:  `"./box/widget/moc.doxa"`,
					},
					{
						Mapper: TokenMapper,
						Type:   ItemImportEnd,
						Value:  "",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
		}

		for _, spec := range specs {
			tests.Header("Running: %+q", spec.Name)

			tokens := make(chan lexer.LexToken, 0)
			lx := lexer.NewAsyncLexer(spec.Name, spec.Input, tokens, TokenMapper)
			go lx.Run(LexDirective)

			var res []lexer.LexToken

			for item := range tokens {
				res = append(res, item)
			}

			if spec.FailExpected {
				if !deepCompare(spec.Expected, res) {
					tests.Info("Received Tokens: %+q\n", res)
					tests.Info("Expected Tokens: %+q\n", spec.Expected)
					tests.Failed("Should have failed with expected tokens from lexer")
				}

				tests.Passed("Should have failed with expected tokens from lexer")
				continue
			}

			if !deepCompare(spec.Expected, res) {
				tests.Info("Received Tokens: %+q\n", res)
				tests.Info("Expected Tokens: %+q\n", spec.Expected)
				tests.Failed("Should have received expected tokens from lexer")
			}
			tests.Passed("Should have received expected tokens from lexer")
		}
	}
}

func TestDoxex_EnumDeclaration(t *testing.T) {
	tests.Header("When parsing enum directive using the doxex format")
	{
		var specs = []TestSpec{
			{
				Name: "declare message directive",
				Input: `
					enum Trees {
						Flat 1
						Undertones 2
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemEnum,
						Mapper: TokenMapper,
						Value:  "Trees",
					},
					{
						Type:   ItemEnumBodyStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Undertones",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "2",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemEnumBodyEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "declare message directive with comment",
				Input: `
					enum Trees {
						Flat 1 # flat tires are flat
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemEnum,
						Mapper: TokenMapper,
						Value:  "Trees",
					},
					{
						Type:   ItemEnumBodyStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " flat tires are flat",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemEnumBodyEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "declare message directive with comment after field",
				Input: `
					enum Trees {
						Flat 1 
						# flat tires are flat
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemEnum,
						Mapper: TokenMapper,
						Value:  "Trees",
					},
					{
						Type:   ItemEnumBodyStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " flat tires are flat",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemEnumBodyEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "declare message directive with comment before field",
				Input: `
					enum Trees {
						# flat tires are flat
						Flat 1 
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemEnum,
						Mapper: TokenMapper,
						Value:  "Trees",
					},
					{
						Type:   ItemEnumBodyStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " flat tires are flat",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemEnumBodyEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
		}

		for _, spec := range specs {
			tests.Header("Running: %+q", spec.Name)

			tokens := make(chan lexer.LexToken, 0)
			lx := lexer.NewAsyncLexer(spec.Name, spec.Input, tokens, TokenMapper)
			go lx.Run(LexDirective)

			var res []lexer.LexToken

			for item := range tokens {
				res = append(res, item)
			}

			if spec.FailExpected {
				if !deepCompare(spec.Expected, res) {
					tests.Info("Received Tokens: %+q\n", res)
					tests.Info("Expected Tokens: %+q\n", spec.Expected)
					tests.Failed("Should have failed with expected tokens from lexer")
				}

				tests.Passed("Should have failed with expected tokens from lexer")
				continue
			}

			if !deepCompare(spec.Expected, res) {
				tests.Info("Received Tokens: %+q\n", res)
				tests.Info("Expected Tokens: %+q\n", spec.Expected)
				tests.Failed("Should have received expected tokens from lexer")
			}
			tests.Passed("Should have received expected tokens from lexer")
		}
	}
}

func TestDoxex_MessageDeclaration(t *testing.T) {
	tests.Header("When parsing message directive using the doxex format")
	{
		var specs = []TestSpec{
			{
				Name: "declare message directive",
				Input: `
					message Hello {
						!name string 3
						age int 2
						addresses list string 1
						status enum Messages 4
						state message State 5
						codes list message Lex 6
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemMessage,
						Mapper: TokenMapper,
						Value:  "Hello",
					},
					{
						Type:   ItemMessageStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "!name",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "string 3",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "age",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "int 2",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "addresses",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "list string 1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "status",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "enum Messages 4",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "state",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "message State 5",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "codes",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "list message Lex 6",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemMessageEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "declare message directive with comment",
				Input: `
					message Trees {
						Flat string 1 # flat tires are flat
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemMessage,
						Mapper: TokenMapper,
						Value:  "Trees",
					},
					{
						Type:   ItemMessageStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "string 1 ",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " flat tires are flat",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemMessageEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "declare message directive with comment after field",
				Input: `
					message Trees {
						Flat string 1
						# flat tires are flat
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemMessage,
						Mapper: TokenMapper,
						Value:  "Trees",
					},
					{
						Type:   ItemMessageStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "string 1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " flat tires are flat",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemMessageEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "declare message directive with comment before field",
				Input: `
					message Trees {
						# flat tires are flat
						Flat string 1
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemMessage,
						Mapper: TokenMapper,
						Value:  "Trees",
					},
					{
						Type:   ItemMessageStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " flat tires are flat",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "string 1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemMessageEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
			{
				Name: "declare message directive with comment in between field",
				Input: `
					message Trees {
						Flat string 1
						# flat tires are flat
						Flat2 string 1
					}
				`,
				Expected: []lexer.LexToken{
					{
						Type:   ItemMessage,
						Mapper: TokenMapper,
						Value:  "Trees",
					},
					{
						Type:   ItemMessageStart,
						Mapper: TokenMapper,
						Value:  "{",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "string 1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemCommentStart,
						Mapper: TokenMapper,
						Value:  "#",
					},
					{
						Type:   ItemComment,
						Mapper: TokenMapper,
						Value:  " flat tires are flat",
					},
					{
						Type:   ItemCommentEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldStart,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemFieldName,
						Mapper: TokenMapper,
						Value:  "Flat2",
					},
					{
						Type:   ItemFieldValue,
						Mapper: TokenMapper,
						Value:  "string 1",
					},
					{
						Type:   ItemFieldEnd,
						Mapper: TokenMapper,
						Value:  "",
					},
					{
						Type:   ItemMessageEnd,
						Mapper: TokenMapper,
						Value:  "}",
					},
					{
						Type:   lexer.ItemEOF,
						Mapper: TokenMapper,
					},
				},
			},
		}

		for _, spec := range specs {
			tests.Header("Running: %+q", spec.Name)

			tokens := make(chan lexer.LexToken, 0)
			lx := lexer.NewAsyncLexer(spec.Name, spec.Input, tokens, TokenMapper)
			go lx.Run(LexDirective)

			var res []lexer.LexToken

			for item := range tokens {
				res = append(res, item)
			}

			if spec.FailExpected {
				if !deepCompare(spec.Expected, res) {
					tests.Info("Received Tokens: %+q\n", res)
					tests.Info("Expected Tokens: %+q\n", spec.Expected)
					tests.Failed("Should have failed with expected tokens from lexer")
				}

				tests.Passed("Should have failed with expected tokens from lexer")
				continue
			}

			if !deepCompare(spec.Expected, res) {
				tests.Info("Received Tokens: %+q\n", res)
				tests.Info("Expected Tokens: %+q\n", spec.Expected)
				tests.Failed("Should have received expected tokens from lexer")
			}
			tests.Passed("Should have received expected tokens from lexer")
		}
	}
}

func deepCompare(spec1, spec2 []lexer.LexToken) bool {
	if len(spec1) != len(spec2) {
		return false
	}
	for ind, spec := range spec1 {
		other := spec2[ind]
		if other.Type == spec.Type && other.Value == spec.Value {
			continue
		}
		return false
	}
	return true
}

package doxex

import (
	"fmt"
	"testing"

	"github.com/influx6/faux/tests"
)

func TestGenerateFile(t *testing.T) {
	pkg, imported, err := GenerateFile("./data/sample.dox")
	if err != nil {
		tests.FailedWithError(err, "Should have successfully generated data")
	}

	fmt.Printf("Package: %q\n", pkg.Name)
	fmt.Printf("Enums: %#v\n", pkg.Enums)
	fmt.Printf("Models: %#v\n", pkg.Models)
	//fmt.Printf("Imported: %#v\n", imported)
	_ = imported
}

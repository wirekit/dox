package doxex

import (
	"errors"
	"io/ioutil"
	"path/filepath"

	"strings"

	"fmt"
	"strconv"

	"regexp"

	"github.com/wirekit/dox/lexer"
	"github.com/wirekit/dox/version"
)

var (
	multiSpace  = regexp.MustCompile("\\s+")
	digitsOnly  = regexp.MustCompile("\\d+")
	lettersOnly = regexp.MustCompile("\\w+")
)

// errors ...
var (
	ErrNoPackageName           = errors.New("no package name provided")
	ErrDuplicateName           = errors.New("type exists with name already")
	ErrInvalidTypeCount        = errors.New("message field types can have parts of either 2, 3 or 4 not lower or higher")
	ErrInvalidUseOfList        = errors.New("message field list type must occur with a type i.e list string, list int")
	ErrInvalidUseOfEnum        = errors.New("message field list type must occur with a type i.e enum Jets, enum Jacks")
	ErrInvalidUseOfMessage     = errors.New("message field message type must occur with a reference name i.e message People, message Socks")
	ErrInvalidFieldType        = errors.New("message field types is unknown")
	ErrInvalidFieldDeclaration = errors.New("message field type declaration is invalid")
	ErrFieldTypeNotFound       = errors.New("message field types is not found")
	ErrFieldTypeNotSupported   = errors.New("message field types is not supported")
	ErrInvalidFieldID          = errors.New("message field type id must be a number")
)

// EnumValue defines a giving enum value type which is part of a
// enum list.
type EnumValue struct {
	ID   int
	Name string
}

// Enum represents a parsed enum declared within a spec.
type Enum struct {
	Name   string
	Values []EnumValue
}

// Spec specifies the data related to a parsed message format.
type Spec struct {
	Name   string
	Fields map[string]SpecField
}

// SpecField defines the data representing a spec field.
type SpecField struct {
	Name   string
	Source string
	ID     int

	IsList   bool
	Required bool
	IsSpec   bool
	IsEnum   bool

	Spec     *Spec
	SpecName string
	Enum     *Enum
	EnumName string
}

// Package defines a struct to represent an declared package.
type Package struct {
	Version          string
	Name             string
	Path             string
	Source           string
	Models           map[string]Spec
	Enums            map[string]Enum
	Imports          map[string]string
	ImportedPackages map[string]Package
}

// GenerateFile will load all source files and referenced/imported package
// and parsing as needed but will not resolve references in Message types.
func GenerateFile(file string) (Package, map[string]Package, error) {
	sourceData, err := ioutil.ReadFile(file)
	if err != nil {
		return Package{}, nil, err
	}

	imported := map[string]Package{}
	pkg, err := generatePackage(string(sourceData), file, imported)
	return pkg, imported, err
}

func generatePackage(source string, sourcePath string, imported map[string]Package) (Package, error) {
	var pkg Package
	pkg.Enums = map[string]Enum{}
	pkg.Models = map[string]Spec{}
	pkg.Imports = map[string]string{}
	pkg.ImportedPackages = imported

	dirPath := filepath.Dir(sourcePath)

	pkg.Path = sourcePath
	pkg.Version = version.DoxVersion
	pkg.Source = source

	parser := New(filepath.Base(sourcePath), string(source))
	versions, pkgs, imps, messages, enums, err := getTokens(parser)
	if err != nil {
		return pkg, err
	}

	// Get the version for giving models.
	for _, ver := range versions {
		if ver.Type == ItemVersion {
			vs := unquote(ver.Value)
			if vs != "" {
				pkg.Version = vs
			}
			break
		}
	}

	// Get the package name from the pkg directive.
	for _, pk := range pkgs {
		if pk.Type == ItemPackageName {
			pkg.Name = unquote(pk.Value)
			break
		}
	}

	// Get the package name from the pkg directive.
	//if pkg.Name == "" {
	//	return pkg, ErrNoPackageName
	//}

	for _, imp := range imps {
		if imp.Type != ItemImportTarget {
			continue
		}

		// if given import has already being loaded, skip it.
		target := unquote(imp.Value)
		targetPath := filepath.Join(dirPath, target)
		pkg.Imports[target] = targetPath

		targetData, err := ioutil.ReadFile(targetPath)
		if err != nil {
			return pkg, err
		}

		if _, ok := imported[targetPath]; !ok {
			item, err := generatePackage(string(targetData), targetPath, imported)
			if err != nil {
				return pkg, err
			}

			imported[targetPath] = item
		}
	}

	for _, enum := range enums {
		var enumSpec Enum
		if enum[0].Type != ItemEnum {
			return pkg, errors.New("invalid enum section found")
		}

		enumSpec.Name = unquote(enum[0].Value)
		for i := 0; i < len(enum[1:]); i++ {
			item := enum[i]
			if item.Type == ItemFieldStart {
				fieldName := enum[i+1]
				fieldValue := enum[i+2]

				var val EnumValue
				val.Name = unquote(fieldName.Value)
				val.ID, err = strconv.Atoi(unquote(fieldValue.Value))
				if err != nil {
					return pkg, fmt.Errorf("error convert enum %q item %q id to integer: %q", enumSpec.Name, val.Name, fieldValue.Value)
				}

				enumSpec.Values = append(enumSpec.Values, val)

				// jump over ItemFieldEnd;
				i += 3
			}
		}

		pkg.Enums[enumSpec.Name] = enumSpec
	}

	for _, mac := range messages {
		var msgSpec Spec
		if mac[0].Type != ItemMessage {
			return pkg, errors.New("invalid mac section found")
		}

		msgSpec.Fields = map[string]SpecField{}
		msgSpec.Name = unquote(mac[0].Value)

		for i := 0; i < len(mac[1:]); i++ {
			item := mac[i]
			if item.Type == ItemFieldStart {
				fieldName := mac[i+1]
				fieldValue := mac[i+2]

				var val SpecField
				val.Name = unquote(fieldName.Value)
				val.Source = multiSpace.ReplaceAllString(strings.TrimSpace(unquote(fieldValue.Value)), " ")

				if strings.HasPrefix(val.Name, "!") {
					val.Name = val.Name[1:]
					val.Required = true
				}

				// Split value parts
				spec := strings.Split(val.Source, " ")
				if len(spec) <= 1 {
					return pkg, ErrInvalidFieldDeclaration
				}

				switch len(spec) {
				case 2:
					specType := strings.ToLower(spec[0])
					switch specType {
					case messsageHeader:
						return pkg, ErrInvalidUseOfMessage
					case enumHeader:
						return pkg, ErrInvalidUseOfEnum
					case listModifier:
						return pkg, ErrInvalidUseOfList
					}

					specID := spec[1]
					if !digitsOnly.MatchString(specID) {
						return pkg, ErrInvalidFieldID
					}
				case 3:
				case 4:
				default:
					return pkg, ErrInvalidTypeCount
				}

				//val.ID, err = strconv.Atoi(unquote(fieldValue.Value))
				//if err != nil {
				//	return pkg, fmt.Errorf("error convert mac %q item %q id to integer: %q", msgSpec.Name, val.Name, fieldValue.Value)
				//}

				msgSpec.Fields[val.Name] = val

				// jump over ItemFieldEnd;
				i += 3
			}
		}

		pkg.Models[msgSpec.Name] = msgSpec
	}

	return pkg, nil
}

func unquote(s string) string {
	return strings.TrimSuffix(strings.TrimPrefix(s, "\""), "\"")
}

func getTokens(parser Parser) (version, pkg, imports []lexer.LexToken, models, enums [][]lexer.LexToken, err error) {
	seenNames := map[string]struct{}{}

	for {
		token, finished := parser.NextToken()
		if token.Type == lexer.ItemError {
			err = errors.New(token.Value)
			return
		}

		if token.Type == lexer.ItemEOF {
			return
		}

		if token.Type >= ItemMetaBegin && token.Type <= ItemMetaEnd {
			if token.Type >= ItemImportBegin && token.Type <= ItemImportEnd {
				imports = append(imports, token)
				continue
			}

			if token.Type >= ItemPackageBegin && token.Type <= ItemPackageEnd {
				pkg = append(pkg, token)
				continue
			}

			version = append(version, token)
			continue
		}

		//if token.Type >= ItemCommentStart && token.Type <= ItemCommentEnd && doComments {
		//		//	comments = append(comments, token)
		//		//	continue
		//		//}

		if token.Type >= ItemDirectiveStart && token.Type <= ItemDirectiveEnd {
			//models = append(models, token)
			var model []lexer.LexToken

			if token.Type == ItemEnum {
				if _, ok := seenNames[token.Value]; ok {
					err = ErrDuplicateName
					return
				}

				model = append(model, token)
				seenNames[token.Value] = struct{}{}

			em:
				for {
					// Load all enum parts before returning to yielding parser;
					token, finished = parser.NextToken()
					if token.Type == lexer.ItemError {
						err = errors.New(token.Value)
						return
					}

					if token.Type == lexer.ItemEOF {
						return
					}

					model = append(model, token)
					if token.Type == ItemEnumBodyEnd {
						break em
					}
				}

				enums = append(enums, model)
				continue
			}

			if token.Type == ItemMessage {
				if _, ok := seenNames[token.Value]; ok {
					err = ErrDuplicateName
					return
				}

				model = append(model, token)
				seenNames[token.Value] = struct{}{}

			mm:
				for {
					// Load all message parts before returning to yielding parser;
					token, finished = parser.NextToken()
					if token.Type == lexer.ItemError {
						err = errors.New(token.Value)
						return
					}

					if token.Type == lexer.ItemEOF {
						return
					}

					model = append(model, token)
					if token.Type == ItemMessageEnd {
						break mm
					}
				}

				models = append(models, model)
				continue
			}

			continue
		}

		if finished {
			return
		}
	}
}

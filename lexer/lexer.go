package lexer

import (
	"errors"
	"fmt"
	"strings"
	"unicode/utf8"
)

const (
	EOF = rune(0)

	// Digits contains all digits values between 0-9.
	Digits = "0123456789"

	// LettersLowerCase contains all alphabet in lowercase.
	LettersLowerCase = "abcdefghijklmnopqrstuvwxyz"

	// LettersUpperCase contains all alphabet in uppercase.
	LettersUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// LettersLowerAndUpperCase contains all alphabet in lowercase and uppercase.
	LettersLowerAndUpperCase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

//************************************************
// NextState
//************************************************

// NextState sets a function type which may return
// another state to be executed.
type NextState func(*Lexer) NextState

//************************************************
// ItemType and lexItem
//************************************************

// ItemType defines a type for tokens.
type ItemType int

// TokenMapper defines a function type which returns a string
// value for a giving ItemType.
type TokenMapper func(ItemType) string

const (
	ItemError ItemType = iota
	ItemEOF
)

// LexToken defines a atomic token parsed from a giving string.
type LexToken struct {
	Type   ItemType
	Value  string
	Mapper TokenMapper
}

// String returns the string representation of the token.
func (i LexToken) String() string {
	switch i.Type {
	case ItemError:
		return "LexError: " + i.Value
	case ItemEOF:
		return "[EOF]"
	}

	if i.Mapper != nil {
		if res := i.Mapper(i.Type); res != "" {
			if i.Value != "" {
				return fmt.Sprintf("{%s:%q}", res, i.Value)
			}
			return res
		}
	}

	return fmt.Sprintf("%q", i.Value)
}

//************************************************
// SyncLexer
//************************************************

// SyncLexer composes a Lexer and exposes a synchronous API for use that
// hides the underline concurrency of the Lexer implementation.
type SyncLexer struct {
	l     *Lexer
	state NextState
}

// NewSyncLexer returns a new instance of a SyncLexer.
func NewSyncLexer(name string, source string, initial NextState, m ...TokenMapper) *SyncLexer {
	var mapper TokenMapper
	if len(m) != 0 {
		mapper = m[0]
	}
	return &SyncLexer{
		state: initial,
		l:     New(name, source, mapper, make(chan LexToken, 2)),
	}
}

// NextToken returns the next token available and a true/false value if
// more tokens is to be expected.
// If lexer has finished then either a EOF or Error
// token is returned depending on final state.
func (l *SyncLexer) NextToken() (token LexToken, finished bool) {
	for {
		select {
		case token = <-l.l.tokens:
			if token.Type == ItemError || token.Type == ItemEOF {
				finished = true
			}
			return
		default:
			l.state = l.state(l.l)
		}
	}
}

//************************************************
// ASyncLexer
//************************************************

// AsyncLexer composes a Lexer and exposes a asynchronous API for use that
// accepts a channel through which tokens will be received and immediately
// kicks off the tokenization process after call to AsyncLexer.Run with the desired
// initial state. It closes the provided channel once no
// more tokens can be provided.
type AsyncLexer struct {
	l *Lexer
}

// NewAsyncLexer returns a new instance of a AsyncLexer.
func NewAsyncLexer(name string, source string, tokens chan LexToken, m ...TokenMapper) *AsyncLexer {
	var mapper TokenMapper
	if len(m) != 0 {
		mapper = m[0]
	}
	lsx := &AsyncLexer{l: New(name, source, mapper, tokens)}
	return lsx
}

// Run exposes a method which should be executed in a go-routine, has
// it will call all states for the lexer from the provided initial passed
// in till there is no possible state to be processed.
func (l *AsyncLexer) Run(initial NextState) {
	for next := initial(l.l); next != nil; {
		next = next(l.l)
	}
	close(l.l.tokens)
}

//************************************************
// Lexer
//************************************************

// Lexer implements the dox tokenizer for parsing dox files.
type Lexer struct {
	name   string // name identifier for source used for error.
	source string // string being scanned
	start  int    // start position of current Item
	pos    int    // current position of lexer
	width  int    // width of last read rune
	err    error
	tokens chan LexToken
	mapper TokenMapper
}

// New returns a new instance of a Lexer with provided name, source and channel and
// initial state.
func New(name string, source string, m TokenMapper, tokens chan LexToken) *Lexer {
	return &Lexer{
		mapper: m,
		name:   name,
		source: source,
		tokens: tokens,
	}
}

// EmitError returns a nil value for NextState to be used
// to terminate the call for next state has an error occurred.
// The Error is sent as a LexToken into the receiving channel.
func (l *Lexer) EmitError(msg string, v ...interface{}) NextState {
	em := fmt.Sprintf(msg, v...)
	l.err = errors.New(em)
	l.tokens <- LexToken{
		Type:   ItemError,
		Mapper: l.mapper,
		Value:  em,
	}
	return nil
}

// Error returns the last emitted error from the lexer.
func (l *Lexer) Error() error {
	return l.err
}

// Emit delivers current string slice of start and pos to
// emitter.
func (l *Lexer) Emit(t ItemType) {
	l.tokens <- LexToken{
		Type:   t,
		Mapper: l.mapper,
		Value:  l.source[l.start:l.pos],
	}
	l.start = l.pos
}

// Next consumes and returns next rune.
func (l *Lexer) Next() rune {
	if l.pos >= len(l.source) {
		l.width = 0
		return EOF
	}

	r, width := utf8.DecodeRuneInString(l.source[l.pos:])
	l.pos += width
	l.width = width
	return r
}

// TakeTill consumes all runes that fall within valid set
// returning after all have being consumed.
func (l *Lexer) TakeTill(set string) {
	for strings.IndexRune(set, l.Next()) >= 0 {
	}
	l.Backup()
}

// TakeIf consumes next rune if its within valid
// char in provided string and returns true/false if
// it successfully consumed it.
func (l *Lexer) TakeIf(set string) bool {
	if strings.IndexRune(set, l.Next()) >= 0 {
		return true
	}
	l.Backup()
	return false
}

// HasRuneIn returns the true/false if giving rune exists within
// current string from current position to length of m of string exists.
func (l *Lexer) HasRuneIn(r rune, m int) bool {
	return l.RuneIndexIn(r, m) >= 0
}

// RuneIndexIn returns the index of giving rune within
// current string from current position to length of m of string.
func (l *Lexer) RuneIndexIn(r rune, m int) int {
	if m > l.Rem() {
		m = l.Rem()
	}

	return strings.IndexRune(l.source[l.pos:m], r)
}

// Rem returns total length of string left to be lexed, which
// includes the current position of lexer. If you want total
// left after current position use Lexer.RemAfter.
func (l *Lexer) Rem() int {
	return len(l.source[l.start:])
}

// RemAfter returns total length of string left after current
// position of the lexer.
func (l *Lexer) RemAfter() int {
	return len(l.source[l.pos:])
}

// RuneIndex returns the index of giving rune within
// current string from current position to end of string.
func (l *Lexer) RuneIndex(r rune) int {
	return strings.IndexRune(l.source[l.pos:], r)
}

// HasRune returns true/false if rune exists in string
// from current position to end of string.
func (l *Lexer) HasRune(r rune) bool {
	return l.RuneIndex(r) >= 0
}

// Jump pushes/adds the giving lexer forward by the provided i value.
// It will set the underline width to the i value to ensure
// Lexer.Backup still works.
func (l *Lexer) Jump(i int) {
	l.pos += i
	l.width = i
}

// Start returns the current start of the current item.
func (l *Lexer) Start() int {
	return l.start
}

// Pos returns the position of the lexer within the current item.
func (l *Lexer) Pos() int {
	return l.pos
}

// PeekString returns the current string from the current position
// of the lexer to the length of m. It does not move the lexer forward.
func (l *Lexer) PeekString(m int) string {
	left := l.RemAfter()
	if m > left {
		m = left
	}

	return l.source[l.pos:m]
}

// PeekTill returns the possible string from the current position of
// the lexer till it finds the rune or nothing if rune was not found.
// It returns at the first occurrence of giving rune. If no occurence
// is found then be aware it will return the remaining string from the
// current position of lexer.
// NOTE: If the rune is EOF then the whole string is returned from current position.
func (l *Lexer) PeekTill(r rune) string {
	if r == EOF {
		return l.source[l.pos:]
	}

	for w, rx := range l.source[l.pos:] {
		if rx != r {
			continue
		}

		return l.source[l.pos : l.pos+w]
	}

	return l.source[l.pos:]
}

// PeekIfOnly returns the string found from the current position of
// the lexer till the index of the rune found, else returns nothing.
// It defers from Lexer.PeekTill which returns whole string from
// lexer position even if index is not found.
// NOTE: If the rune is EOF then the whole string is returned from current position.
func (l *Lexer) PeekIfOnly(r rune) string {
	if r == EOF {
		return l.source[l.pos:]
	}

	for w, rx := range l.source[l.pos:] {
		if rx != r {
			continue
		}

		return l.source[l.pos : l.pos+w]
	}

	return ""
}

// PeekConsumed returns the current part of the string consumed.
// It returns a string from the current start of the last emitted
// to Lexer position of the last consumed.
func (l *Lexer) PeekConsumed() string {
	if l.pos > l.start {
		m := l.pos
		return l.source[l.start:m]
	}
	return ""
}

// Peek returns a rune with moving forward.
func (l *Lexer) Peek() rune {
	next := l.Next()
	l.Backup()
	return next
}

// BackupBy moves the lexer cursor back giving amount
// this can not be reversed, hence if you backup said amount
// you must also utilize the Lexer.Jump as well to reverse
// within said amount.
func (l *Lexer) BackupBy(n int) {
	l.pos -= n
}

// Backup paces pack one step to last rune read.
func (l *Lexer) Backup() {
	l.pos -= l.width
}

// Ignore moves forward without consuming current rune.
func (l *Lexer) Ignore() {
	l.start = l.pos
}

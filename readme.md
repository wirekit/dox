Dox
------
[![Go Report Card](https://goreportcard.com/badge/github.com/wirekit/dox)](https://goreportcard.com/report/github.com/wirekit/dox)
[![Travis CI](https://travis-ci.org/wirekit/dox.svg?master=branch)](https://travis-ci.org/wirekit/dox)

Dox is a type protocol format which is used to generate both golang code and json type headers to allow simple and straight
binary format transferal between languages while requiring only the understanding of the binary format being used.

Dox binary format removes all headers and uses number tags to identify types and their respective fields which allows
receiving ends regardless of language to be able too read and deserialize data appropriately.

## Status
Dox is currently a personal experiment and is still in beta.

## Install

```bash
go get github.com/wirekit/dox
```

## CLI
Dox provides a binary which will be installed on `go get` and can be used to generate said types and json meta.
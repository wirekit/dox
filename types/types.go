package types

import "errors"

// ErrInvalidType is returned when type is invalid or not
// matching any constant of DataType.
var ErrInvalidType = errors.New("type unknown and invalid")

// DataType defines a int8 type to represent set of types.
type DataType int8

// constants of DataType.
const (
	Invalid DataType = iota
	Int
	Int8
	Int16
	Int32
	Int64
	Uint
	Uint8
	Uint16
	Uint32
	Uint64
	String
	Time
	Bytes
	Boolean
	Float32
	Float64
	List
)

// GetType returns the associated type
func GetType(v string) (DataType, error) {
	switch v {
	case "string":
	case "int":
	case "int8":
	case "int16":
	case "int32":
	case "int64":
	case "uint":
	case "uint8":
	case "uint16":
	case "uint32":
	case "uint64":
	case "boolean":
	case "time":
	case "bytes":
	case "list":
	}
}

// String returns the string representation of each type.
func (d DataType) String() string {
	switch d {
	case String:
		return "string"
	case Int:
		return "int"
	case Int8:
		return "int8"
	case Int16:
		return "int16"
	case Int32:
		return "int32"
	case Int64:
		return "int64"
	case Uint:
		return "uint"
	case Uint8:
		return "uint8"
	case Uint16:
		return "uint16"
	case Uint32:
		return "uint32"
	case Uint64:
		return "uint64"
	case Boolean:
		return "bool"
	case Float32:
		return "float32"
	case Float64:
		return "float64"
	case Bytes:
		return "bytes"
	case List:
		return "list"
	case Time:
		return "time"
	default:
		return "invalid"
	}
}

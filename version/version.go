package version

const (
	// DoxVersion sets the current version of the dox parser format.
	DoxVersion = "1.0"
)
